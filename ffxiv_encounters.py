#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 18:22:24 2021

@author: FFXIV Progress Plotter
"""

"""
    Encounters:
"""

import pandas as pd

# Spell times and names are taken from Syldris Lia's timelines.

encounter_3b2_timeline = [
    ["73", "Flood of Obscurity 1"],
    ["101", "Wide-angle/Anti-air 1"],
    ["159", "Wide-angle/Anti-air 2"],
    ["198", "Empty Plane 1"],
    ["279", "Deluge of Darkness 1"],
    ["302", "Art of Darkness (Summon)"],
    ["389", "Towers"],
    ["445", "Second Art of Darkness 2"],
    ["477", "Empty Plane 2"],
    ["600", "Enrage"]
    ]

encounter_3b3_timeline = [
    ["86", "Dualspell"],
    ["159", "Giga Slash + Shadows"],
    ["210", "First Orbs"],
    ["240", "4 Shadows + Orbs"],
    ["279", "Implosion*4"],
    ["384", "Voidgate 1-1"],
    ["402", "Voidgate 1-2"],
    ["442", "KB Orbs 1"],
    ["504", "Shackled Apart Orbs"],
    ["577", "Voidgate 2-1"],
    ["593", "Voidgate 2-2"],
    ["632", "KB Orbs Final"],
    ["680", "Enrage"]
    ]

encounter_3b4_timeline = [
    ["47", "Break, Strike, Bound"], 
    ["148", "Shifting Sky"],
    ["263", "Break, Strike, Bound, Raidwide"],
    ["351", "Sundered Sky"],
    ["396", "Break"],
    ["438", "Turn of the Heavens"],
    ["493", "Prismatic Deception"],
    ["570", "Cycle of Faith, Raidwide 1"],
    ["608", "Cycle of Faith, Raidwide 2"],
    ["646", "Cycle of Faith, Raidwide 3"],
    ["690", "Enrage"]
    ]

encounter_3b5_1_timeline = [
    ["43", "Junction Shiva/Titan 1"],
    ["105", "Diamond Dust"],
    ["141", "Icicles"],
    ["178", "Earthen Fury"],
    ["205", "Second Titan boulders"],
    ["298", "Sculptures"],
    ["361", "Lions"],
    ["437", "Junction Shiva/Titan 2"],
    ["465", "Junction Shiva/Titan 3"],
    ["507", "Enrage"]
    ]

encounter_3b5_2_timeline = [
    ["53", "Dark Water III/Eruption"],
    ["102", "Basic Relativity"],
    ["163", "Singular Apocalypse"],
    ["224", "Intermediate Relativity"],
    ["281", "Dual Apocalypse"],
    ["351", "Advanced Relativity"],
    ["413", "Triple Apocalypse"],
    ["471", "Terminal Relativity"],
    ["534", "Enrage"]
    ]

encounter_3b2_timeline = pd.DataFrame(encounter_3b2_timeline, columns = ["time", "spell"])
encounter_3b3_timeline = pd.DataFrame(encounter_3b3_timeline, columns = ["time", "spell"])
encounter_3b4_timeline = pd.DataFrame(encounter_3b4_timeline, columns = ["time", "spell"])
encounter_3b5_1_timeline = pd.DataFrame(encounter_3b5_1_timeline, columns = ["time", "spell"])
encounter_3b5_2_timeline = pd.DataFrame(encounter_3b5_2_timeline, columns = ["time", "spell"])

encounter_empty_timeline = pd.DataFrame(columns = ["time, spell"])

class Encounter_data:
    def __init__(self, floor, timeline):
        self.__floor = floor
        self.__timeline = timeline

    def get_timeline(self, boss_name : str):
        return self.__timeline
    
    def get_floor(self, boss_name : str):
        return self.__floor


class E12S_data(Encounter_data):
    P1_boss_name = {
        "en": "eden's promise",
        "fr": "promesse d'éden",
        "de": "edens verheißung",
        "jp": "プロミス・オブ・エデン"
        }
    P2_boss_name = {
        "en": "oracle of darkness",
        "fr": "prêtresse des ténèbres",
        "de": "orakel der dunkelheit",
        "jp": "闇の巫女"
        }

    def get_timeline(self, boss_name : str):
        if boss_name in self.P1_boss_name.values():
            return encounter_3b5_1_timeline
        elif boss_name in self.P2_boss_name.values():
            return encounter_3b5_2_timeline
        else:
            return encounter_empty_timeline
        
    def get_floor(self, boss_name : str):
        if boss_name in self.P1_boss_name.values():
            return "E12S-P1"
        elif boss_name in self.P2_boss_name.values():
            return "E12S-P2"
        else:
            return "E12S"
        
encounters = {
    "355": Encounter_data("E1S", encounter_empty_timeline),
    "356": Encounter_data("E2S", encounter_empty_timeline),
    "357": Encounter_data("E3S", encounter_empty_timeline),
    "358": Encounter_data("E4S", encounter_empty_timeline),
    "38a": Encounter_data("E5S", encounter_empty_timeline),
    #"38b": Encounter_data("E6S", encounter_empty_timeline),
    "38c": Encounter_data("E7S", encounter_empty_timeline),
    "38d": Encounter_data("E8S", encounter_empty_timeline),
    "3b2": Encounter_data("E9S", encounter_3b2_timeline),
    "3b3": Encounter_data("E10S", encounter_3b3_timeline),
    "3b4": Encounter_data("E11S", encounter_3b4_timeline),
    "3b5": E12S_data("E12S", encounter_empty_timeline)
    }
