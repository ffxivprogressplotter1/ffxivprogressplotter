#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 18:50:14 2021

@author: FFXIV Progress Plotter
"""

import os
import argparse

import pandas as pd
from tqdm import tqdm

import ffxiv_parser
import ffxiv_plotter

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--directory", help="Path to a directory holding FFXIV .log files")
    args = parser.parse_args()
    if args.directory:
        directory = args.directory
    else:
        directory = "FFXIVLogs"

    data = []
    pbar = tqdm(os.scandir(directory))
    for entry in pbar:
        pbar.set_description("Reading file %s" % entry.path)
        if entry.path.endswith(".log"):
            pull_list = ffxiv_parser.get_pull_list(entry.path)

            for pull in pull_list:
                data.append([
                    pull.encounter_id,
                    pull.party,
                    pull.boss_name,
                    pull.isClear(),
                    pull.boss_hp,
                    pull.start_time,
                    pull.end_time,
                    pull.getPullDuration(),
                    pull.getWipeReason()
                    ])

    df = pd.DataFrame(data, columns=["encounter_id", "party_comp", "boss_name",
                                     "is_clear", "%boss_hp", "start_time",
                                     "end_time", "duration", "wipe_reason"])
    df["start_time"] = pd.to_datetime(df["start_time"])
    df["end_time"] = pd.to_datetime(df["end_time"])
    df["boss_name"] = df["boss_name"]
    gby_encounter = df.groupby(by="boss_name")



    for group in gby_encounter:
        print(group[0])
        ffxiv_plotter.generate_report(group[0], group[1], directory)



if __name__ == "__main__":
    main()