#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 18:29:36 2021

@author: FFXIV Progress Plotter
"""
import os

import datetime as dt

import pandas as pd
from tqdm import tqdm

import plotly.graph_objects as go
import gif

import ffxiv_encounters


def plot_progress(group_id, group):
    g_df = group.sort_values(by="start_time", ascending=True, ignore_index=True)
    g_df.reset_index(inplace=True)
    
    # Split into sessions of 12 consecutive hours
    S = g_df.end_time
    sessions = [g.reset_index(drop=True)
                for i, g in g_df.groupby([(S - S.iloc[0])
                                          .astype('timedelta64[12h]')])]
    x_wipes = []
    x_clears = []
    xticks_pos = []
    xlabels = []
    for i in range (len(sessions)):
        sessions[i].sort_values(by="start_time", ascending=True, inplace=True)
        offset = 5*i + len(x_wipes)
        x_wipes += [y for y in range(offset, offset + len(sessions[i]))]
        
        for index, row in sessions[i].iterrows():
            if row["is_clear"]:
                x_clears.append(offset + index)
                x_wipes.remove(offset + index)
        
        xticks_pos.append(offset + len(sessions[i])/2)
        session_start = sessions[i]["start_time"].iloc[0]
        xlabels.append(session_start.date())

    y_wipes = 100 - g_df.loc[g_df["is_clear"] == False]["%boss_hp"]
    
    fig = go.Figure()
    fig.add_trace(go.Bar(
            x = x_wipes,
            y = y_wipes,
            width = 0.33,
            name = "Wipes",
            marker = dict(
                line = dict(
                    color = "blue"
                )
            )
        )
    )

    if len(x_clears) > 0:
        fig.add_trace(go.Bar(
                x = x_clears,
                y = [100 for i in range (0,len(x_clears))],
                width = 0.33,
                name = "Clears",
                marker = dict(
                    line = dict(
                        color = "red"
                    )
                )
            )
        )
        
    if len(x_clears) > 0:
        first_clear_date = g_df.loc[g_df["is_clear"] == True].iloc[0]["end_time"].date()
        title = group_id
        title += "<br>" + str(len(g_df)) + " logged pulls (first clear on " + str(first_clear_date)
        title += " after " + str(g_df.loc[g_df["is_clear"] == True].iloc[0]["index"]) + " pulls)"
    else:
        title = group_id + "<br>" + str(len(g_df)) + " logged pulls"


    fig.update_layout(
        xaxis = dict(
            title_text = "Sessions",
            tickmode = 'array',
            tickvals = xticks_pos,
            ticktext = xlabels,
            tickangle = 315
        ),
        yaxis = dict(
            title_text = "%hp removed"
        ),
        title = dict(
            text = title
        )
    )
    
    #fig.show()
    return fig



def plot_duration(group_id, group, encounter_data):
    g_df = group.sort_values(by="duration", ascending=True)
    clears = g_df.loc[g_df["is_clear"] == True]
    wipes = g_df.loc[g_df["is_clear"] == False]


    encounter_timeline = encounter_data.get_timeline(group_id)
    if not encounter_timeline.empty :
        x_spell_times = encounter_timeline["time"].astype(int)
        x_spell_names = encounter_timeline["spell"]
    else:
        x_spell_times = []
        x_spell_names = []


    fig = go.Figure()

    fig.add_trace(go.Bar(
            x = wipes["duration"],
            y = [x for x in range (0, len(wipes))],
            width = 0.33,
            orientation = "h",
            name = "Wipes",
            marker = dict(
                line = dict(
                    color = "blue"
                )
            )
        )
    )

    fig.add_trace(go.Bar(
            x = clears["duration"],
            y = [x for x in range (len(wipes), len(wipes) + len(clears))],
            width = 0.33,
            orientation = "h",
            name = "Clears",
            marker = dict(
                line = dict(
                    color = "red"
                )
            )
        )
    )

    for i in range(0, len(x_spell_times)):
        fig.add_vline(x=int(x_spell_times[i]), line_width=1, fillcolor="purple",
                      annotation_text=x_spell_names[i], annotation_position="bottom right",
                      annotation_textangle=270)

    title = group_id
    title += "<br>" + str(len(g_df)) + " logged pulls (" + str(len(clears)) + " clears, highlighted in red"
    title += ", total fight time: " + str(dt.timedelta(seconds=g_df["duration"].sum())) + ")"
    fig.update_layout(
        xaxis = dict(
            title_text = "Pull Duration"
        ),
        yaxis = dict(
            title_text = "Pulls (ordered by duration)"
        ),
        title = dict(
            text = title
        )
    )

    #fig.show()
    return fig



def __build_sankey_values(timeline, link_sources, df):
    """
        Set the values for each link by counting the wipe reasons.

        The first value is the number of wipes before the first mechanic.
        The second (i = 1) is the number of pulls that have seen the first mechanic.
        The third (i = 2) is the number of wipes caused by that first mechanic.
        The fourth (i = 3) is the number of pulls that have seen the second mechanic.

        After each mechanic, we have to update the value of all prior odd links
            to keep the flow consistent (see the j loop).
    """
    value = [0 for i in range(0, len(link_sources))]
    value[0] = len(df.loc[df["wipe_reason"] == ""])

    if timeline.empty:
        return value

    for i in range(1, len(timeline)):
        value[2*(i)] = len(df.loc[df["wipe_reason"] == timeline["spell"][i-1]])
        for j in range(0, 2*(i)):
            if j % 2 == 1:
                value[j] += len(df.loc[df["wipe_reason"] == timeline["spell"][i-1]])

    """
        All that is left to do is to update with the number of enrages, which the
            the last mechanic of each fight.
    """
    value[-1] = len(df.loc[df["wipe_reason"] == timeline["spell"].iloc[-1]])
    for j in range (0, len(value) - 1):
        if j % 2 == 1:
            value[j] += len(df.loc[df["wipe_reason"] == timeline["spell"].iloc[-1]])

    return value



def __build_sankey_flow(timeline, df):
    longest_pull = df["duration"].max()
    seen_mechanics = timeline.loc[timeline["time"] - 3 <= longest_pull + 5]

    """
        Build the list of nodes and their position.
        Pulls -> Wipe -> Spell_1 -> Wipe -> Spell_2 -> Wipe -> ... -> Enrage -> Wipe
    """
    nodes = []
    nodes.append("Pulls")                                                 # Starting point
    for spell in seen_mechanics["spell"]:
        nodes.append("Wipe")
        nodes.append(spell)
    nodes.append("Wipe")

    if seen_mechanics.empty:
        source = [0]     # From Pulls, to Wipe
        target = [1]
    else:
        source = [0, 0]     # From Pulls, to Wipe, and from Pulls to Spell_1
        target = [1, 2]

    """
        Build the list of links between nodes.
        Odds position links are edges between a spell and the next one.
        Even position links are edges leading from the previous spell to the wipes on it.

        Exception on the first one (0th link) are the wipes between the
            beginning of the pull and the first mechanic.

        The "enrage" node will be added by target.append(spell_index+2).
    """
    for spell in range(0, len(seen_mechanics["spell"]) - 1):
        spell_index = nodes.index(seen_mechanics["spell"][spell])     # Get the index of the spell in nodes list
        source.append(spell_index)
        target.append(spell_index + 1)       # Spell_n -> wipe, is in even position
        source.append(spell_index)
        target.append(spell_index + 2)       # spell_n -> spell_n+1, is in odd position
        
    source.append(len(nodes) - 2)
    target.append(len(nodes) - 1)

    return nodes, source, target, seen_mechanics


def __position_sankey_nodes(seen_mechanics, nodes, target, value):
    size = len(seen_mechanics) + 2
    x = [1/size, 2/size]
    y = [0.4, 0.6]
    
    if len(seen_mechanics) == 0:
        return x, y

    for i in range(0, len(target)):
        dest = nodes[target[i]]
        if value[i] > 0:
            if dest == "Wipe":
                x.append(x[-1])
                y.append(0.4)
            else:
                x.append(x[-1] + 1/size)
                y.append(0.6)

    return x, y



def __add_clears_sankey(timeline, nodes, x, y, source, target, value, df):
    source_wipes = len(source)
    clears = df.loc[df["is_clear"] == True]

    if len(clears) == 0:
        return nodes, source, target, x, y, value

    if not "Clear" in nodes:
        nodes.append("Clear")
        x.append(x[-1])
        y.append(1)

    for duration in clears["duration"]:
        passed_mechanics = timeline.loc[timeline["time"] <= int(duration)]
        latest_mechanic = passed_mechanics.loc[passed_mechanics["time"].idxmax()]
        latest_mechanic_node_idx = nodes.index(latest_mechanic["spell"])

        already_cleared_there = -1
        for i in range(source_wipes, len(source)):
            if latest_mechanic_node_idx == source[i]:
                already_cleared_there = i
                break
        if already_cleared_there > 0:
            value[i] += 1
        else:
            source.append(latest_mechanic_node_idx)
            target.append(nodes.index("Clear"))
            value.append(1)

        for i in range(0, latest_mechanic_node_idx):
            if i % 2 == 1:
                value[i] += 1

    return nodes, source, target, x, y, value



@gif.frame
def __plot_sankey_frames(group_id, timeline, df, i):
    wipes = df.head(i).loc[df["is_clear"] == False]

    nodes, source, target, seen_mechanics = __build_sankey_flow(timeline, wipes)
    value = __build_sankey_values(seen_mechanics, source, wipes.head(i))
    x, y = __position_sankey_nodes(seen_mechanics, nodes, target, value)
    nodes, source, target, x, y, value = __add_clears_sankey(timeline, nodes, x, y, source, target, value, df.head(i))

    """
        Setting up links color
    """
    link_color = ["#bab3b3" for i in range(0, len(source))]
    latest_pull = df.head(i).iloc[[-1]]
    if latest_pull["is_clear"].item() == True:
        passed_mechanics = timeline.loc[timeline["time"] <= int(latest_pull["duration"].item())]
        latest_mechanic = passed_mechanics.loc[passed_mechanics["time"].idxmax()]
        spell_index = nodes.index(latest_mechanic["spell"])
        for j in range(1, spell_index):
            if j % 2 == 1:
                link_color[j] = "#35c470"
        for j in range(0, len(source)):
            if source[j] == spell_index and target[j] == (len(nodes) - 1):
                link_color[j] = "#35c470"
    else:
        wipe_reason = wipes.head(i).iloc[[-1]]["wipe_reason"].values[0]
        if wipe_reason == "":
            spell_index = 0
            link_color[0] = "#c44339"
        else:
            spell_index = nodes.index(wipe_reason)
            for j in range(1, spell_index):
                if j % 2 == 1:
                    link_color[j] = "#c44339"
            link_color[spell_index-1] = "#c44339"
            link_color[spell_index] = "#c44339"

    """
        Now that all values needed to build the Sankey diagram are computed,
            let's finally plot it, shall we?
    """
    fig = go.Figure()
    fig.add_trace(go.Sankey(
        arrangement = "snap",
        orientation = "h",
        node = dict(
            x = x,
            y = y,
            pad = 15,
            thickness = 10,
            line = dict(color = "black", width = 0.1),
            label = nodes,
            color = "#3d63a1"
        ),
        link = dict(
            source = source,
            target = target,
            value = value,
            color = link_color
        )
    ))

    date = df.head(i).iloc[[-1]]["end_time"].item().date()
    title = group_id + ", pull outcomes"
    title += "<br>" + str(date) + ", pull n°" + str(i)

    fig.update_layout(
        width=3000,
        height=720,
        title = dict(
            text = title
        )
    )

    return fig



def plot_sankey(group_id, group, encounter_data):
    g_df = group.sort_values(by="duration", ascending=True)
    wipes = g_df.loc[g_df["is_clear"] == False]

    timeline = encounter_data.get_timeline(group_id)
    if timeline.empty:
        return None
    timeline["time"] = pd.to_numeric(timeline["time"])
    timeline = timeline.sort_values(by="time", ascending=True, ignore_index=True)

    nodes, source, target, seen_mechanics = __build_sankey_flow(timeline, wipes)
    value = __build_sankey_values(seen_mechanics, source, wipes)
    
    x, y = __position_sankey_nodes(seen_mechanics, nodes, target, value)
    nodes, source, target, x, y, value = __add_clears_sankey(timeline, nodes, x, y, source, target, value, g_df)

    """
        Now that all values needed to build the Sankey diagram are computed,
            let's finally plot it, shall we?
    """
    fig = go.Figure()
    fig.add_trace(go.Sankey(
        arrangement = "snap",
        orientation = "h",
        node = dict(
            x = x,
            y = y,
            pad = 20,
            thickness = 10,
            line = dict(color = "black", width = 0.1),
            label = nodes,
            color = "blue"
        ),
        link = dict(
            source = source,
            target = target,
            value = value
        )
    ))
    
    title = group_id + ", pull outcomes"
    title += "<br>" + str(len(group)) + " logged pulls"

    fig.update_layout(
        title = dict(
            text = title
        )
    )


    return fig



def animate_sankey(group_id, group, encounter_data, directory):
    g_df = group.sort_values(by="end_time", ascending=True)

    timeline = encounter_data.get_timeline(group_id)
    if timeline.empty:
        return None
    timeline["time"] = pd.to_numeric(timeline["time"])
    timeline = timeline.sort_values(by="time", ascending=True, ignore_index=True)

    frames = []
    for i in tqdm(range(1, len(g_df))):
        frames.append(__plot_sankey_frames(group_id, timeline, g_df, i))
    filename = os.path.join(directory, encounter_data.get_floor(group_id) + "_sankey.gif")
    gif.save(frames, filename, duration=100)
    print("Generated file %s" % filename)



def generate_report(group_id, group, directory):
    encounter_id = group.iloc[0]["encounter_id"]
    encounter_data = ffxiv_encounters.encounters[encounter_id]

    
    report_elements = []
    progress_fig = plot_progress(group_id, group)
    duration_fig = plot_duration(group_id, group, encounter_data)
    sankey_fig = plot_sankey(group_id, group, encounter_data)
    animate_sankey(group_id, group, encounter_data, directory)
    
    report_elements.append(progress_fig)
    report_elements.append(duration_fig)
    report_elements.append(sankey_fig)
    
    filepath = os.path.join(directory, encounter_data.get_floor(group_id) + "_report.html")
    
    with open(filepath, 'w') as f:
        for fig in report_elements:
            f.write(fig.to_html(
                full_html=False,
                include_plotlyjs=True,
                auto_play=False,
                animation_opts = dict(
                    frame = dict(
                        duration = 1000
                    ),
                    fromcurrent = True,
                    transition = dict(
                        duration = 1000,
                        easing = "linear-in-out"
                    )
                )
            ))
    print("Generated file %s" % filepath)