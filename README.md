FFXIVProgressPlotter is a Python tool that parses your FFXIV ACT log files to plot your pulls on (*for the moment*) current tier savage raids.
It was made with the help of this [documentation](https://github.com/quisquous/cactbot/blob/main/docs/LogGuide.md).

![E9S progression plot](./examples/E9S_progress.png)
![E9S duration plot](./examples/E9S_duration.png)
![E9S pull outcomes plot](./examples/E9S_sankey.png)

# Requirements
1. `python >= 3.7.0`
2. pandas
3. plotly
4. gif (for animating plotly graphs)
5. tqdm

# Usage
## UNIX systems
`python3 main.py --directory="/path/to/log/directory"`

## Windows 10
I recommend using [PowerShell](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-7.1).
Please check your PowerShell version beforehand, you might run into issues if using a version prior to 6.0.

0. Install the latest version of [python 3](https://www.python.org/downloads/windows/).
    - You can also install a version of Python 3 from the Microsoft Store, see the [documentation](https://docs.microsoft.com/en-us/windows/python/scripting#install-python)
1. In a PowerShell terminal, install the required python packages:
    ```
        pip install plotly gif tqdm pandas --user
        pip install gif[plotly] --user
    ```
2. Download the source code of ffxivprogressplotter. In a PowerShell terminal, navigate into the unzipped project directory.
    - Alternatively, if you have installed git, you can clone the repository.
3. `python main.py --directory="C:\path\to\log\directory"`

# Known Bugs & Issues
- E6S is not yet parsed because it is a multi-boss fight, and because I want to get the boss HP%. I might or might not fix this in the future.
- Checkpointed fights might not work as intended (see issue #1).
- Feel free to open an Issue if you find any bug or have a suggestion.
