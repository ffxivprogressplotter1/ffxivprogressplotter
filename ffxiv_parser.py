#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 18:26:36 2021

@author: FFXIV Progress Plotter
"""
import re
import time
import datetime as dt

import ffxiv_encounters


timestamp_format = "%Y-%m-%dT%H:%M:%S"


"""
    Instance:
        - Corresponds to a raid instance
        - Party members can be detected with event_id 03, their object id always begins with 10
        - In contrary, object id of NPCs always begins with 40
"""
class Instance:
    def __init__(self, log, encounter_id):
        self.pull_list = []
        self.party = set()
        self.encounter_id = encounter_id
        self.__parse_log(log)

    def __parse_log(self, log):
        lines = log

        in_fight = False
        data = []
        start = ""
        boss_id = ""
        for i in range(0, len(lines)):
            if self.__is_setting_primary(lines[i]):
                self.primary_player = lines[i].split("|")[2]
            elif self.__is_adding_player(lines[i]):
                combatant_id = lines[i].split("|")[2]
                if combatant_id.startswith("10") and len(self.party) < 8:
                    self.party.add(combatant_id)
            elif self.__is_attack_on_enemy(lines[i]):
                if not in_fight:
                    start = lines[i]
                    in_fight = True
                    boss_id = start.split("|")[6]
            elif self.__is_fight_end(lines[i], boss_id):
                if in_fight:
                        pull = Pull(data, start, lines[i], self.party, self.encounter_id)
                        self.pull_list.append(pull)
                        in_fight = False
                        data = []
                        boss_id = ""

            if in_fight:
                data.append(lines[i])

    """
        True if the line is setting the primary player (i.e. log proprietary)
    """
    def __is_setting_primary(self, line):
        if line.startswith("02"):
            return True;
        return False;

    """
        True if the line is logging the spawn of a player character
    """
    def __is_adding_player(self, line):
        if line.startswith("03"):
            combatant_id = line.split("|")[2]
            if combatant_id.startswith("10"):
                return True;
        return False;

    """
        True if the line is logging an attack from a player against an enemy
    """
    def __is_attack_on_enemy(self, line):
        if line.startswith("21"):         # 21: Executed ability
            caster_id = line.split("|")[2]
            target_id = line.split("|")[6]
            if caster_id.startswith("10") and target_id.startswith("40"):
                return True;
        return False;

    """
        True if the line is logging a fight end

        It looks like checkpoints do not trigger a 33 event, thus we need to find
            the moment the boss gets removed and track its HP.
        # TODO: Actually check if it is enough to guarantee a wipe or a clear
    """
    def __is_fight_end(self, line, boss_id):
        if line.startswith("33"):
            outcome = line.split("|")[3]
            if outcome == "40000003" or outcome == "40000005":
                return True;

        """
            If the line is logging the removal of an object, and if that object
                is the boss, then we are going to check if its HP.
                object_current_hp == 0 indicates a clear
                object_current_hp == 1 probably indicates a phase transition into
                    a checkpoint
        """
        if line.startswith("04"):
            splitted = line.split("|")
            object_id = splitted[2]
            object_current_hp = splitted[11]
            if object_id.casefold() == boss_id.casefold() and int(object_current_hp) < 2:
                return True

        return False;



"""
    Pull:
        - Should begin when the primary player launches an ability on a non-player target
        - Should end when event_id 33 of type 40000003 (Victory) or 40000005
            (Fade-out screen, which most likely indicates a wipe) is found
        - Must specify who is the main enemy
        # TODO : how to support fight with multiple bosses (e.g. E6S)
"""
class Pull:
    def __init__(self, log, start, end, party, encounter_id):
        self.start = start
        self.__boss_id = start.split("|")[6]      # This is instance-dependent
        self.boss_name = start.split("|")[7].casefold()
        self.start_time = start.split("|")[1].split(".")[0]
        self.end = end
        self.end_time = end.split("|")[1].split(".")[0]
        self.party = party
        self.encounter_id = encounter_id

        self.boss_hp = self.__compute_boss_hp(log)

    def isClear(self):
        if self.end.startswith("33"):
            outcome = self.end.split("|")[3]
            if outcome == "40000003":
                return True;
        elif self.end.startswith("04"):
            object_current_hp = self.end.split("|")[11]
            if int(object_current_hp) < 2:
                return True;
        return False;

    def getPullDuration(self):
        start_timestamp = time.mktime(dt.datetime
                                      .strptime(self.start_time, timestamp_format)
                                      .timetuple())
        end_timestamp = time.mktime(dt.datetime
                                    .strptime(self.end_time, timestamp_format)
                                    .timetuple())

        return end_timestamp - start_timestamp

    def getWipeReason(self):
        duration = self.getPullDuration()
        timeline = ffxiv_encounters.encounters[self.encounter_id].get_timeline(self.boss_name)
        timeline = timeline.astype({"time": "int32"}, copy=True)

        if self.isClear() or timeline.empty:
            return ''

        first_mechanic = timeline.loc[timeline["time"].idxmin()]
        if first_mechanic["time"] - 3 > duration + 5:
            return ''

        # Add a little bit of padding to catch possible small variations in the timeline
        passed_mechanics = timeline.loc[timeline["time"] - 3 <= duration + 5]
        latest_mechanic = passed_mechanics.loc[passed_mechanics["time"].idxmax()]

        return latest_mechanic["spell"]


    """
        Computes the boss HP right before the wipe, using the last ability that
            targeted it
        Returns 0 if it was a clear
    """
    def __compute_boss_hp(self, log):
        if self.isClear():
            return 0
        else:
            for i in range(len(log)-1, 0, -1):
                if log[i].startswith("21"):         # 21: Executed ability
                    target_id = log[i].split("|")[6]
                    if target_id == self.__boss_id:
                        boss_current_hp = log[i].split("|")[24]
                        boss_total_hp = log[i].split("|")[25]

                        return float(boss_current_hp) * 100 / float(boss_total_hp)



def get_pull_list(filepath : str):
    pull_list = []
    f = open(filepath, "r", encoding="utf8")
    data = []

    in_instance = False
    current_line = f.readline()
    encounter_id = ""
    while current_line:
        if re.match(r"^01.*$", current_line):
            location_id = current_line.split("|")[2]
            if location_id in ffxiv_encounters.encounters.keys():
                in_instance = True
                encounter_id = location_id
            else:
                if in_instance:
                    instance = Instance(data, encounter_id)
                    pull_list += instance.pull_list
                    data = []
                    in_instance = False

        if in_instance:
            data.append(current_line)
        current_line = f.readline()

    if in_instance:
        instance = Instance(data, encounter_id)
        pull_list += instance.pull_list
        data = []
        in_instance = False

    f.close()

    return pull_list;
